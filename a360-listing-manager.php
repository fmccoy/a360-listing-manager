<?php

namespace A360\Listing;

/*
Plugin Name: A360 Listing Manager
Plugin URI: http://www.whatsyouranthem.com
Description: Description
Version: 0.1
Author: Frank McCoy
Author URI: http://www.whatsyouranthem.com
*/

/**
 * Copyright (c) `date "+%Y"` Frank McCoy. All rights reserved.
 *
 * Released under the GPL license
 * http://www.opensource.org/licenses/gpl-license.php
 *
 * This is an add-on for WordPress
 * http://wordpress.org/
 *
 * **********************************************************************
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * **********************************************************************
 */

require_once( A360_VENDOR_DIR . '/autoload.php');

define("LM_PLUGIN_DIR", dirname(__FILE__));

class ListingManager{

	public function __construct(){
		$this->init();
	}

	public function init(){
		$plugin = SetupPlugin::init();
		add_action( 'wp_enqueue_scripts', array( $this, 'loadStyles' ) );
	}

	public function loadStyles(){
		wp_enqueue_style('lm-plugin-style', LM_PLUGIN_DIR . '/assets/plugin.css');
	}
}

$listing = new ListingManager;