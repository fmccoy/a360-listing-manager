<?php

namespace A360\Listing;

use A360\Core;
use A360\Core\Plugin;
use A360\Core\ContentType;
use A360\Core\FieldGroup;
use A360\Core\Field;
use A360\Core\Shortcode;
use A360\Core\Metabox;
use A360\Core\Taxonomy;


class SetupPlugin extends Plugin{
	/**
	 * Register Custom Post Types
	 * 
	 * @return
	 */
	public static function register_content_types(){
		// Create a content type
		$type_listing = new ContentType('listing', 'Listings', 'Listing');

		// Add a new field group (i.e. WordPress metabox) for the content type
		$group = new FieldGroup('listing-details', 'Listing_Details', $type_listing );

		// Add fields to the field group
		new Field\Text( 'name', 'Name', $group );
		new Field\Email( 'email', 'Email', $group );
		new Field\Url( 'url', 'URL', $group );
		new Field\Image( 'logo', 'Logo', $group );
	}

	/**
	 * Register Custom Taxonomies
	 * 
	 * @return
	 */
	public static function register_taxonomies(){

		/**
		 * Departments Taxonomy
		 * 
		 */
		$tax_departments_id = 'departments';
		$tax_departments_types = array( 'post', 'listing');
		$tax_departments_labels = array(
			'name' => 'Departments',
			'singular_name' => 'Department',
			'menu_name' => 'Departments'
		);
		$tax_departments_args = array(
			'show_ui' => true, 
			'show_in_menu' => true ,
			'show_in_nav_menus' => true
		);
		
		new Taxonomy\Hierarchical(
			$tax_departments_id,
			$tax_departments_types,
			$tax_departments_labels,
			$tax_departments_args			
		);
	}

	/**
	 * Register Short Codes
	 * 
	 * Call the "register" method of any classes that extend "Shortcode" here
	 * 
	 * @return
	 */
	public static function register_shortcodes(){
		// Code
	}

	/**
	 * Register Meta Boxes
	 *
	 * Call the "register" method of any classes that extend "Metabox" here
	 * 
	 * @return [type] [description]
	 */
	public static function register_metaboxes(){
		//ListingInfoMetabox::register();
	}

	public static function register_scripts(){
		wp_enqueue_style('a360-lm-style', LM_PLUGIN_DIR . '/assets/css/plugin.css');
	}
}
